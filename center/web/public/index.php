<?php
/**
 * Created by PhpStorm.
 * User: mengkang <i@mengkang.net>
 * Date: 2017/10/28 下午9:04
 */

define("ROOT", dirname(__DIR__));

include ROOT . "/mysql/DbConfig.php";
include ROOT . "/mysql/DbConnection.php";
include ROOT . "/mysql/SqlExecute.php";

class Center
{

    public $action = 'index';

    /**
     * Center constructor.
     */
    public function __construct()
    {
        if (isset($_GET['a'])) {
            $this->action = $_GET['a'];
        }

        include_once ROOT . "/view/header.php";
    }

    public function index()
    {
        if (isset($_GET['name'])) {
            $sql = "select * from services where `name` like ? group by `name`";
            $list = SqlExecute::getAll($sql, [$_GET['name']]);
        }else{
            $list = [];
            $_GET['name'] = '';
        }

        include_once ROOT . "/view/index.php";
    }

    public function detail()
    {

        if (isset($_GET['type']) && $_GET['type'] == 'consumer') {
            $sql = "select * from subscriber where service=?";

        } else {
            $sql = "select * from services where `name`=?";
        }
        $list = SqlExecute::getAll($sql, [$_GET['name']]);
        include_once ROOT . "/view/detail.php";
    }

    /**
     * 调用链路
     */
    public function chain()
    {

    }

    /**
     * 服务测试
     */
    public function test()
    {
        if (isset($_GET['name'])) {
            $sql = "select * from services where `name`=?";
            $list = SqlExecute::getAll($sql, [$_GET['name']]);
        }else{
            $list = [];
            $_GET['name'] = '';
        }

        include_once ROOT . "/view/test.php";
    }

    public function selectMethod(){

        // 一般都是固定的端口，我们这里仅为测试
        $res = file_get_contents("http://localhost:8081/rpc.php");
        $res = json_decode($res,true);
        $provider = $res['provider'];


        $list = [];
        foreach ($provider as $service => $item){
            foreach ($item as $v){
                $list[] = $service.".".$v['method'];
            }
        }

        include_once ROOT."/view/select.php";
    }

    /**
     * 权重规则设置
     */
    public function weight()
    {
        if (isset($_GET['name'])) {
            $sql = "select * from services where `name` like ?";
            $list = SqlExecute::getAll($sql, [$_GET['name']]);
        }else{
            $list = [];
            $_GET['name'] = '';
        }

        include_once ROOT . "/view/weight.php";
    }



    public function __destruct()
    {
        include_once ROOT . "/view/footer.php";
    }


}

$center = new Center();
$action = isset($_GET['a']) ? $_GET['a'] : "index";
$center->$action();

