<?php
/**
 * Created by PhpStorm.
 * User: mengkang <i@mengkang.net>
 * Date: 2017/10/29 上午11:39
 */
define("ROOT", dirname(__DIR__));

include ROOT . "/mysql/DbConfig.php";
include ROOT . "/mysql/DbConnection.php";
include ROOT . "/mysql/SqlExecute.php";

class Api
{
    public function setWeight()
    {
        $id = $_POST['id'];
        $weight = $_POST['weight'];

        $sql = "update services set weight=? where id=?";
        SqlExecute::execute($sql, [$weight, $id]);

        // 查询出该服务的名称
        $sql = "select name from services where id=?";
        $name = SqlExecute::getValue($sql, [$id]);

        // 调动服务治理中心给消费者推送最新的配置
        $client = new swoole_client(SWOOLE_SOCK_TCP);
        $client->connect("127.0.0.1", 10000, 0.5);
        $client->send(json_encode([
            'action' => 'pushConfig',
            'data' => [
                'service' => $name
                ],
            ])
        );

        echo $id;
    }
}

$center = new Api();
$action = isset($_GET['a']) ? $_GET['a'] : "index";
$center->$action();