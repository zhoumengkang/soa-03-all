<?php

/**
 * Created by IntelliJ IDEA.
 * User: zhoumengkang
 * Date: 15/9/23
 * Time: 下午11:56
 */
class SqlExecute
{
    private static $debug    = false;
    public  static $db       = null;
    public  static $dbSelect = null;

    /**
     * 开启debug
     */
    public static function setDebug(){
        if(PHP_SAPI == 'cli'){
            self::$debug = true;
        }else if(APP_DEBUG == true || $_SESSION){
            self::$debug = true;
        }
    }

    public static function init($dbSelect){
        if (self::$dbSelect != $dbSelect) {
            if ( self::$db != null ) {
                self::$db->close();
                self::$db = null;
            }
        }

        self::$dbSelect = $dbSelect;

        if ( self::$db == null ) {
            DbConfig::getConnectInfo($dbSelect);
            self::$db = DbConnection::getInstance(DbConfig::$dsn, DbConfig::$user, DbConfig::$pass);
        }
    }

    public static function exceptionHandler(Exception $e,$sql){
        if (self::$debug === true) {
            var_dump($e->getMessage(), $sql);
        }
        return false;
    }

    /**
     * 根据Sql语句, 查询并返回所有数据行
     *
     * @param   string  $sql        SQL语句
     * @param   int     [$binds]    绑定数组
     * @param   string  $dbSelect   数据库类型
     * @param   string  [$style]    SQL查询类型
     * @return  array
     */
    public static function getAll($sql, $binds = null, $dbSelect = MK_MYSQL_READ, $style = PDO::FETCH_ASSOC)
    {
        try {
            self::init($dbSelect);
            return self::$db->getAll($sql, $binds, $style);
        } catch (Exception $e) {
            return self::exceptionHandler($e,$sql);
        }
    }

    /**
     * 根据分页获取数据
     *
     * @param        $sql
     * @param null   $binds
     * @param int    $page
     * @param int    $pageSize
     * @param bool   $countSql
     * @param string $limitPage
     * @param string $dbSelect
     * @return array|bool
     */
    public static function getAllPage($sql, $page = 1, $pageSize = 20, $binds = null, $dbSelect = MK_MYSQL_READ, $countSql = false, $limitPage = '')
    {
        $rs = array();
        if ($limitPage != '') {
            $rs['info']['count'] = $pageSize * $limitPage;
        } else {
            if ($countSql === false) {
                $countSql = preg_replace(array('/SELECT.*?FROM /Asi', '/ORDER BY .*/'), array('SELECT COUNT(*) FROM ', ''), $sql);
            }

            if (is_numeric($countSql)) {
                $rs['info']['count'] = $countSql;
            } else {
                $rs['info']['count'] = self::getValue($countSql, $binds, $dbSelect);
            }
        }

        if ($rs['info']['count'] !== false) {
            if ($rs['info']['count'] < $pageSize)      { $pages = 1; }
            elseif ($rs['info']['count'] % $pageSize)  { $pages = intval($rs['info']['count']/$pageSize)+1; }
            else                                        { $pages = intval($rs['info']['count']/$pageSize); }

            if (!isset($page))  { $page =1; }
            if ($page < 1)      { $page =1; }
            if ($page > $pages) { $page = $pages; }

            $rs['info']['pages'] = $pages;
            $rs['info']['page_size'] = $pageSize;
            $rs['info']['page'] = $page;

            if ($rs['info']['count'] > 0){
                $sql .= ' limit ' . ($page-1)*$pageSize . ',' . $pageSize;
            }
            $rs['item'] = self::getAll($sql, $binds, $dbSelect);
            return $rs;
        }

        return false;
    }

    /**
     * 根据Sql语句, 查询并返回所有数据行中的VALUE值
     *
     * @param        $sql
     * @param null   $binds
     * @param string $dbSelect
     * @return mixed
     */
    public static function getColumn($sql, $binds=null, $dbSelect=MK_MYSQL_READ)
    {
        try {
            self::init($dbSelect);
            return self::$db->getColumn($sql, $binds);
        } catch (Exception $e) {
            return self::exceptionHandler($e,$sql);
        }
    }

    /**
     * 根据Sql语句, 查询并返回一行数据
     *
     * @param        $sql
     * @param null   $binds
     * @param string $dbSelect
     * @param        $style
     * @return mixed
     */
    public static function getOne($sql, $binds=null, $dbSelect=MK_MYSQL_READ, $style=PDO::FETCH_ASSOC)
    {
        try {
            self::init($dbSelect);
            return self::$db->getOne($sql, $binds, $style);
        } catch (Exception $e) {
            return self::exceptionHandler($e,$sql);
        }
    }

    /**
     * 根据Sql语句, 查询并返回一数据行中的VALUE值
     *
     * @param        $sql
     * @param null   $binds
     * @param string $dbSelect
     * @return mixed
     */
    public static function getValue($sql, $binds = null, $dbSelect = MK_MYSQL_READ)
    {
        try {
            self::init($dbSelect);
            return self::$db->getValue($sql, $binds);
        } catch (Exception $e) {
            return self::exceptionHandler($e,$sql);
        }
    }

    /**
     * 执行Sql语句
     *
     * @param $sql
     * @param string $dbSelect
     * @param null $binds
     * @return mixed
     */
    public static function execute($sql, $binds=null, $dbSelect=MK_MYSQL_WRITE)
    {
        try {
            self::init($dbSelect);
            return self::$db->execute($sql, $binds);
        } catch (Exception $e) {
            return self::exceptionHandler($e,$sql);
        }
    }

    public static function getInsertId()
    {
        return self::$db->getInsertId();
    }

    public static function close()
    {
        self::$db->close();
    }
}