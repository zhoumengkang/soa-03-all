<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <h2>搜索结果</h2>
    <form class="bs-example bs-example-form" data-example-id="simple-input-groups">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="服务名 ： UserService" name="name" value="<?php echo $_GET['name'];?>">
        </div>
    </form>
    <br>
    <h2>服务器列表</h2>


    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>服务</th>
                <th>ip</th>
                <th>端口</th>
                <th>应用名</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

                <?php foreach($list as $item){ ?>
                <tr>
                    <td><?php echo $item['name']; ?></td>
                    <td><?php echo $item['ip']; ?></td>
                    <td><?php echo $item['port']; ?></td>
                    <td><?php echo $item['app_name']; ?></td>
                    <td><div class="input-group">
                            权重：<input class="form-control col-sm-2" type="text" name="weight" value="<?php echo $item['weight'];?>" >
                            <button data-id="<?php echo $item['id']; ?>" class="btn btn-success my-2 my-sm-0">提交</button>
                        </div>
                    </td>
                </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
    <script>
        $(function () {
            $(".btn").click(function () {
                $.post(
                    "/api.php?a=setWeight",
                    {
                        id:$(this).data("id"),
                        weight:$(this).siblings("input").val()
                    },
                    function (res) {
                        alert("设置成功");
                    },
                    "html"
                );
            })
        })
    </script>
</main>
