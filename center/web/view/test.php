<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <h2>搜索结果</h2>
    <form class="bs-example bs-example-form" data-example-id="simple-input-groups">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="服务名 ： UserService" name="name" value="<?php echo $_GET['name'];?>">
        </div>
    </form>
    <h2>服务器列表</h2>


    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>服务</th>
                <th>ip</th>
                <th>端口</th>
                <th>应用名</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

                <?php foreach($list as $item){ ?>
                <tr>
                    <td><?php echo $item['name']; ?></td>
                    <td><?php echo $item['ip']; ?></td>
                    <td><?php echo $item['port']; ?></td>
                    <td><?php echo $item['app_name']; ?></td>
                    <td><a href="?a=selectMethod&id=<?php echo $item['id']; ?>">选择方法测试</a></td>
                </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>

</main>
