<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <h2>具体信息</h2>

    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <td>应用名</td>
                <td>xxx</td>
            </tr>
            <tr>
                <td>所属人</td>
                <td><a target="_blank" href="https://mengkang.net/">周梦康</a></td>
            </tr>
            <tr>
                <td>开发负责人</td>
                <td><a target="_blank" href="https://mengkang.net/">周梦康</a></td>
            </tr>
            </tbody>
        </table>
    </div>

    <h2>服务器列表</h2>

    <ul class="nav nav-pills">
        <li role="presentation" style="margin-right: 10px;"><a class="btn btn-info" href="#">服务提供者</a></li>
        <li role="presentation"><a class="btn" href="#">服务消费者</a></li>
    </ul>
    <br>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ip</th>
                <th>端口</th>
                <th>序列化方式</th>
                <th>超时时间</th>
            </tr>
            </thead>
            <tbody>

                <?php foreach($list as $item){ ?>
                <tr>
                    <td><?php echo $item['ip']; ?></td>
                    <td><?php echo $item['port']; ?></td>
                    <td>json</td>
                    <td>2000ms</td>
                </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>

    <h2>元数据</h2>



</main>
