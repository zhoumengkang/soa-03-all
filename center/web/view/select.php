<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <h2>选择方法</h2>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>服务名</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach($list as $item){ ?>
                <tr>
                    <td><?php echo $item; ?></td>
                    <td><a href="?a=test&name=<?php echo $item; ?>" class="btn btn-info my-2 my-sm-0">测试</a></td>
                </tr>
            <?php } ?>

            </tbody>
        </table>
    </div>
</main>
