<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <h2>搜索结果</h2>
    <form class="bs-example bs-example-form" data-example-id="simple-input-groups">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="服务名 ： UserService" name="name" value="<?php echo $_GET['name'];?>">
        </div>
    </form>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>服务名</th>
<!--                <th>分组</th>-->
                <th>所属应用</th>
                <th>详情</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

                <?php foreach($list as $item){ ?>
                <tr>
                    <td><?php echo $item['name']; ?></td>
<!--                    <td>xxx</td>-->
                    <td><?php echo $item['app_name']; ?></td>
                    <td>
                        <a href="?a=detail&name=<?php echo $item['name']; ?>"  class="btn btn-success my-2 my-sm-0">详情</a>
                    </td>
                    <td><a href="?a=test&name=<?php echo $item['name']; ?>" class="btn btn-info my-2 my-sm-0">测试</a></td>
                </tr>
                <?php } ?>

            </tbody>
        </table>
    </div>
</main>
