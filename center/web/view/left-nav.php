<nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
    <ul class="nav nav-pills flex-column">
        <li class="nav-item">
            <a class="nav-link <?php if($this->action == 'index'){ echo "active";}?>" href="?a=index">服务查询</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php if($this->action == 'chain'){ echo "active";}?>" href="#">服务报表</a>
        </li>
        <li class="nav-item">
            <a class="nav-link <?php if($this->action == 'test' || $this->action == 'selectMethod' ){ echo "active";}?>" href="?a=test&name=UserService">服务测试</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link" >服务治理 </a>
            <ul>
                <li><a class="nav-link <?php if($this->action == 'weight'){ echo "active";}?>" href="?a=weight&name=UserService">权重规则</a></li>
                <li><a class="nav-link" href="#">机房规则</a></li>
                <li><a class="nav-link"  href="#">服务鉴权</a></li>
                <li><a class="nav-link"  href="#">服务路由</a></li>
                <li><a class="nav-link"  href="#">服务归组</a></li>
            </ul>
        </li>
    </ul>
</nav>