# soa-03-all
```
├── README.md
├── center
│   ├── main.php 
│   └── web
│       ├── mysql
│       │   ├── DbConfig.php
│       │   ├── DbConnection.php
│       │   └── SqlExecute.php
│       ├── public                  配置了nginx http://localhost:8082/
│       │   ├── api.php
│       │   └── index.php
│       └── view
│           ├── detail.php
│           ├── footer.php
│           ├── header.php
│           ├── index.php
│           ├── left-nav.php
│           ├── select.php
│           ├── test.php
│           ├── top-nav.php
│           └── weight.php
├── consumer-demo
│   ├── config
│   │   ├── center.php
│   │   ├── consumer.php
│   │   └── provider.php
│   ├── main.php
│   ├── services
│   │   └── MessageService.php
│   └── web                         配置了nginx http://localhost:9601/
│       ├── index.php
│       └── rpc.php
├── provider-demo
│   ├── client.php
│   ├── config
│   │   ├── center.php
│   │   ├── consumer.php
│   │   └── provider.php
│   ├── main.php
│   ├── services
│   │   ├── StudentUserService.php
│   │   └── UserService.php
│   └── web                         配置了nginx http://localhost:8081/
│       └── rpc.php
├── register_center.sql
└── rpc
    ├── Dispatcher.php
    ├── Request.php
    ├── Response.php
    └── RpcClient.php
```

# 说明
1. rpc 通信原理请参考 

    > https://gitee.com/zhoumengkang/soa-01-php-client
    
    > https://gitee.com/zhoumengkang/soa-01-php-service
    
    > https://gitee.com/zhoumengkang/soa-01-java-client
    
2. 服务注册的demo 请参考 https://gitee.com/zhoumengkang/soa-02-all

3. 上面的几个标注的 web目录我均使用的 nginx 主要是为了代码简单，也可以直接像我们 https://gitee.com/zhoumengkang/soa-02-all 中使用 swoole 来启动 web 服务