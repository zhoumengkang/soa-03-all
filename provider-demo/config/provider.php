<?php
/**
 * Created by PhpStorm.
 * User: mengkang <i@mengkang.net>
 * Date: 2017/10/29 上午8:51
 */
return [
    'app_name' => 'user-center',                      //服务名称
    'ip' => "0.0.0.0",
    'port' => 9512,
    'services' => [
        'UserService' => [
            // 其他业务参数
//            'version' => 'xxx',
//            'group' => 'xxx',
//              ...
        ],
        'StudentUserService'  => [
            // 其他业务参数
//            'version' => 'xxx',
//            'group' => 'xxx',
//              ...
        ],
    ]
];
