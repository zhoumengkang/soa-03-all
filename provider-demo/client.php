<?php
/**
 * User: 楚松 <mengkang.zmk@alibaba-inc.com>
 * Date: 17/10/27
 * Time: 上午10:58
 */

$client = new swoole_client(SWOOLE_SOCK_TCP);
$client->connect("127.0.0.1", 9512, 0.5);
$content = "12345";
$requestId = 123;
$str = pack('NN', $requestId, strlen($content)) . $content;
$client->send($str);
$res = $client->recv();