<?php
/**
 * Created by PhpStorm.
 * User: mengkang <i@mengkang.net>
 * Date: 2017/10/29 上午8:51
 */
return [
    'app_name' => 'msm-center',                      //服务名称
    'ip' => "0.0.0.0",
    'port' => 9600,
    'notify_port' => 9601,
    'services' => [
        'MessageService' => [

        ],
    ]
];
