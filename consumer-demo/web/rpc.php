<?php
/**
 * Created by PhpStorm.
 * User: mengkang <i@mengkang.net>
 * Date: 2017/10/29 上午8:42
 */

define('ROOT', dirname(__DIR__));

function getCenter()
{
    return include ROOT . "/config/center.php";
}

function getProvider()
{
    $config = include ROOT . "/config/provider.php";
    $list = array_keys($config['services']);

    $res = [];
    foreach ($list as $service) {
        include ROOT . "/services/" . $service . ".php";
        $class = new ReflectionClass($service);
        $methods = $class->getMethods(ReflectionMethod::IS_STATIC
            | ReflectionMethod::IS_PUBLIC);

        foreach ($methods as $item) {
            $params = [];
            foreach ($item->getParameters() as $parameter) {
                $params[] = $parameter->getName();
            }
            $res[$service][] = [
                'method' => $item->name,
                'params' => $params,
            ];
        }
    }
    return $res;
}

function getConsumer()
{
    if (file_exists(ROOT . "/config/consumer.php")) {
        return include ROOT . "/config/consumer.php";
    }
    return [];
}

$res = [
    'center' => getCenter(),
    'provider' => getProvider(),
    'consumer' => getConsumer()
];

echo json_encode($res);