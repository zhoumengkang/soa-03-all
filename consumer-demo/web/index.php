<?php
/**
 * Created by PhpStorm.
 * User: mengkang <i@mengkang.net>
 * Date: 2017/10/29 下午3:41
 */
define('ROOT', dirname(__DIR__));
include_once ROOT."/services/MessageService.php";
include_once dirname(ROOT)."/rpc/RpcClient.php";
include_once dirname(ROOT)."/rpc/Dispatcher.php";
include_once dirname(ROOT)."/rpc/Request.php";
include_once dirname(ROOT)."/rpc/Response.php";
include dirname(ROOT) . "/center/web/mysql/DbConfig.php";
include dirname(ROOT) . "/center/web/mysql/DbConnection.php";
include dirname(ROOT) . "/center/web/mysql/SqlExecute.php";

$res = MessageService::sendMessage(1,'xxx');
