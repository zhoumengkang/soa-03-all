# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.27)
# Database: register_center
# Generation Time: 2017-11-01 12:47:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table service_list
# ------------------------------------------------------------

DROP TABLE IF EXISTS `service_list`;

CREATE TABLE `service_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL COMMENT '服务名称',
  `ip` char(15) NOT NULL COMMENT '服务ip',
  `port` mediumint(8) NOT NULL COMMENT '服务端口',
  `status` tinyint(1) NOT NULL COMMENT '运行状态',
  `rate` smallint(4) NOT NULL COMMENT '权重',
  `registerTime` int(10) NOT NULL COMMENT '注册时间',
  `startTime` int(10) NOT NULL COMMENT '启动时间',
  `dropTime` int(10) NOT NULL COMMENT '停止时间',
  `registerKey` varchar(100) NOT NULL COMMENT '从哪个注册服务器注册的',
  `serverType` smallint(4) NOT NULL DEFAULT '0' COMMENT '服务类型',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_ip_port` (`ip`,`port`) USING BTREE,
  KEY `idx_name` (`name`) USING BTREE,
  KEY `registerKey` (`registerKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='服务表';



# Dump of table services
# ------------------------------------------------------------

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '服务名称',
  `app_name` varchar(100) DEFAULT NULL,
  `ip` char(15) NOT NULL COMMENT '服务ip',
  `port` mediumint(8) NOT NULL COMMENT '服务端口',
  `notify_port` mediumint(8) NOT NULL DEFAULT '9601',
  `status` tinyint(1) NOT NULL COMMENT '运行状态',
  `weight` smallint(4) NOT NULL COMMENT '权重',
  `register_time` int(10) NOT NULL COMMENT '注册时间',
  `start_time` int(10) NOT NULL COMMENT '启动时间',
  `drop_time` int(10) NOT NULL COMMENT '停止时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_ip_port` (`ip`,`port`) USING BTREE,
  KEY `idx_name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='服务表';

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;

INSERT INTO `services` (`id`, `name`, `app_name`, `ip`, `port`, `notify_port`, `status`, `weight`, `register_time`, `start_time`, `drop_time`)
VALUES
	(1,'UserService','user-center','0.0.0.0',8081,9700,1,1000,0,0,0),
	(3,'UserService','user-center','0.0.0.0',9512,9800,1,200,0,0,0),
	(4,'MessageService','msm-center','0.0.0.0',9600,9602,1,100,0,0,0);

/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subscriber
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subscriber`;

CREATE TABLE `subscriber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(100) NOT NULL DEFAULT '' COMMENT '服务名',
  `app_name` varchar(100) NOT NULL DEFAULT '' COMMENT '订阅者',
  PRIMARY KEY (`id`),
  KEY `serviceName` (`service`),
  KEY `subcriber` (`app_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `subscriber` WRITE;
/*!40000 ALTER TABLE `subscriber` DISABLE KEYS */;

INSERT INTO `subscriber` (`id`, `service`, `app_name`)
VALUES
	(1,'UserService','msm-center');

/*!40000 ALTER TABLE `subscriber` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
